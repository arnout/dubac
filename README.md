*dubac* is a simple backup tool that copies files from source directories to a
target directory. It is meant for making backups of large files that usually
don't change but that are often renamed - e.g. a collection of pictures and
videos that gets (re)organised over time. The key distinguishing feature is
that a manifest is stored with hashes of the files that have been backed up
before; this makes deduplication easy and cheap. In addition, the hash used is
a _subsha_, a partial hash of (large) files.

Installation
============

Make sure the `attrs` python package is installed, e.g. with
`pip install attrs`.

Copy dubac and subsha.py somewhere in your `PATH`, or create a symlink from
somewhere in your `PATH` to dubac.


Usage
=====

Run `dubac --help` for details.


Contributing
============

The official repository is at https://gitlab.com/arnout/dubac
Use merge requests to propose changes.

When making changes, make sure you also update the test suite and run the
tests. Just run test.sh.


