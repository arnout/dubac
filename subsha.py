#!/usr/bin/env python
# This code is licensed under the MIT license. See COPYING for details.

"""Calculate a hash of a a subset of a file.

This partial hash allows to quickly find out if a file has changed.
Large files (almost) never change somewhere in the middle. They change by
adding or removing something at the beginning or at the end. When a few
bytes are changed without changing the file size, these bytes will be either
near the beginning or near the end of the file.

The subsha hashes the first and the last chunk of the file, and regular smaller
chunks in the middle. The chunks in the middle are added to detect corruptions
e.g. due to a disk crash."""

from __future__ import print_function
import attr, os, hashlib, re, binascii
from collections import defaultdict

def relpath(path, basedir = None):
    start = basedir or os.curdir
    return os.path.relpath(os.path.abspath(path), os.path.abspath(start))

def cache_attr(default = None):
    return attr.ib(default = default, init = False, hash = False, repr = False, cmp = False)

@attr.s
class subsha(object):
    filename = attr.ib()
    hash = attr.ib()
    size = attr.ib()
    mtime = attr.ib(cmp=False, hash=False)

    def __str__(self):
        return "%s %12d %16d %s" % (binascii.hexlify(self.hash).decode(), self.size, self.mtime, self.filename)

    def hasChanged(self, basedir):
        """Compare if the file has changed compared to the current file in basedir.

        Only size and mtime are checked, it is assumed changed if they are different."""
        filename = os.path.join(basedir, self.filename)
        s = os.stat(filename)
        if s.st_size != self.size or int(s.st_mtime*1e6) != self.mtime:
            return True
        else:
            return False

    @classmethod
    def fromfilename(cls, filename, basedir = None, blobsize = 1024*1024, chunksize = 4096):
        s = os.stat(filename)
        hash = hashlib.sha1()
        with open(filename, 'rb') as f:
            hash.update(f.read(blobsize))
            while f.tell() < s.st_size - 2*blobsize:
                f.seek(int(2*blobsize/3), 1)
                hash.update(f.read(chunksize))
            if f.tell() < s.st_size:
                hash.update(f.read())
        return cls(relpath(filename, basedir), hash.digest(), s.st_size, int(s.st_mtime*1e6))

@attr.s
class subshaSet(object):
    basedir = attr.ib()
    blobsize = attr.ib(default = 1024*1024)
    chunksize = attr.ib(default = 4096)
    _subshas_by_name = cache_attr(default = attr.Factory(dict))
    _subshas_by_hash = cache_attr(default = attr.Factory(lambda: defaultdict(set)))

    def add(self, newobj):
        if not isinstance(newobj, subsha):
            newobj = subsha.fromfilename(newobj, self.basedir, self.blobsize, self.chunksize)

        self._subshas_by_name[newobj.filename] = newobj
        self._subshas_by_hash[newobj.hash].add(newobj)
        return newobj

    def remove(self, oldsubsha):
        del self._subshas_by_name[oldsubsha.filename]
        s = self._subshas_by_hash[oldsubsha.hash]
        s.remove(oldsubsha)
        if not s:
            del self._subshas_by_hash[oldsubsha.hash]

    def getByName(self, filename):
        return self._subshas_by_name.get(filename)

    def getByHexHash(self, hexhash):
        return self.getByHash(binascii.unhexlify(hexhash))

    def getByHash(self, hash):
        # Use get here, we don't want to add the empty set just by querying a hash
        return self._subshas_by_hash.get(hash, set())

    def __str__(self):
        return '\n'.join(map(str, self._subshas_by_name.values())) + '\n'

    def save(self, filename):
        if not os.path.isabs(filename):
            filename = os.path.join(self.basedir, filename)
        with open(filename, 'wt') as o:
            o.write(str(self))

    @classmethod
    def fromfilename(cls, filename):
        s = cls(basedir=os.path.dirname(os.path.abspath(filename)))
        subshaline = re.compile('(?P<hash>[0-9a-fA-F]{32,})\\s+' +
                                '(?P<size>[0-9]+)\\s+' +
                                '(?P<mtime>[0-9]+)\\s+' +
                                '(?P<filename>[^\n]+)\n')
        with open(filename, 'rt') as f:
            for line in f:
                m = subshaline.match(line)
                if m: # Ignore any non-matching lines
                    d = m.groupdict()
                    hash = binascii.unhexlify(d['hash'])
                    size = int(d['size'])
                    mtime = int(d['mtime'])
                    filename = d['filename']
                    s.add(subsha(filename, hash, size, mtime))
        return s

def walk(basedirOrSubshaSet, newcallback = None, updatecallback = None, unchangedcallback = None):
    if isinstance(basedirOrSubshaSet, subshaSet):
        sss = subshaSet(basedirOrSubshaSet.basedir)
        oldsss = basedirOrSubshaSet
    else:
        sss = subshaSet(basedirOrSubshaSet)
        oldsss = None
    for dirpath, dirnames, filenames in os.walk(sss.basedir):
        for filename in filenames:
            filename = os.path.join(dirpath, filename)
            oldsubsha = oldsss and oldsss.getByName(filename)
            if not oldsubsha:
                newsubsha = sss.add(filename)
                if newcallback:
                    newcallback(newsubsha)
            elif oldsubsha.hasChanged(sss.basedir):
                newsubsha = sss.add(filename)
                if updatecallback:
                    updatecallback(oldsubsha, newsubsha)
                elif newcallback:
                    newcallback(newsubsha)
            else: # Unchanged
                newsubsha = sss.add(oldsubsha)
                if unchangedcallback:
                    unchangedcallback(oldsubsha)
                elif newcallback:
                    newcallback(newsubsha)
    return sss

if __name__ == '__main__':
    import sys
    for f in sys.argv[1:]:
        if os.path.isdir(f):
            s = subshaSet(f)
        else:
            s = subshaSet.fromfilename(f)
        walk(s, newcallback = print)
