#! /bin/bash -e
# This code is licensed under the MIT license. See COPYING for details.

cleanup() {
    for dir; do
        test "$dir" && rm -rf "$dir"
    done
}

register_cleanup() {
    trap "cleanup $@" EXIT INT TERM HUP
}

genfile() {
    local size="$1"
    local nonce="$2"

    yes | head -$size | tr -d '\n' | tr y \\$(printf '%03o' 0x$(printf "%6x%s" "$size" "$nonce" | md5sum | cut -c -2))
}

create_testdir() {
    local testdir="$1"
    local dir count size

    for dir in source1 source2 target; do
        mkdir "$testdir/$dir"
    done

    count=0
    for size in \
            $((1000*1000*9)) \
            4095 \
            4096 \
            4097 \
            20 \
            0 \
            $((1024*1024 - 4096)) \
            $((1024*1024)) \
            $((1024*1024 + 4095)); do
        let count++ || :
        mkdir "$testdir/source1/dir${count}"
        genfile $size > "$testdir/source1/dir${count}/file1_$size"
    done
    (cd "$testdir/source1"; find -type f | xargs sha1sum) >> "$testdir/sha1sums"

    count=0
    for size in \
            $((1000*1000)) \
            $((2*1024*1024)) \
            $((2*4096)) \
            $((2*1024*1024 + 4095)) \
            $((2*1024*1024 + 4097)); do
        let count++ || :
        genfile $size > "$testdir/source2/file2_${count}_$size"
    done
    (cd "$testdir/source1"; find -type f | xargs sha1sum) >> "$testdir/sha1sums"

    count=0
    for size in \
            $((1000*1000*9)) \
            $((2*1024*1024)) \
            $((2*4096)) \
            0 \
            1 \
            $((1024*1024 + 4095)) \
            $((2*4096)) \
            $((1024*1024 + 4097)); do
        let count++ || :
        mkdir "$testdir/target/dir${count}"
        genfile $size > "$testdir/target/dir${count}/file3_$size"
    done
    # Some files that exist already
    genfile $((2*1024*1024 + 4095)) > "$testdir/target/file2_4_2101247"
    genfile 20 > "$testdir/target/dir5/file1_20"
    # Some files that exist already with different content but same size
    mkdir "$testdir/target/dir9"
    genfile $((1024*1024 + 4095)) different > "$testdir/target/dir9/file1_1052671"
    genfile 1 different > "$testdir/target/dir1/file4_1"
    # Note file1_9_1052671 will be different so eliminate it here
    (cd "$testdir/source1"; find -type f | grep -v file1_9_1052671 | xargs sha1sum) >> "$testdir/sha1sums"
}

register_cleanup '$testdir'
testdir=$(mktemp -d testdir.XXXXXX)
create_testdir "$testdir"

${PYTHON:=python} ${0%/*}/dubac "$testdir/target" "$testdir/source1" "$testdir/source2"

# check sha1sums
(cd "$testdir/target"; sha1sum --check --quiet) < "$testdir/sha1sums"

# dubac.subsha files must exist
for d in source1 source2 target; do
    echo "Checking $d dubac.subsha file"
    test -e "$testdir/$d/dubac.subsha" || { echo "No dubac.subsha for $d" 1>&2; exit 1; }
    # dubac.subsha must contain all and only the files in that dir (except itself)
    (cd "$testdir/$d"; find -type f) | sort | sed 's%^./%%; /^dubac.subsha/d' > "$testdir/$d.files"
    awk '{ print $4; }' "$testdir/$d/dubac.subsha" | sort | diff -u - "$testdir/$d.files"
done

